#create namespaces

kubectl create ns red

kubectl create ns blue

#create blue and red pods

kubectl apply -f bluepod.yaml

kubectl apply -f redpod.yaml

#create blue and red services

kubectl apply -f bluesvc.yaml

kubectl apply -f redsvc.yaml

#create configmap for proxy

kubectl apply -f proxyconfmap.yaml

#create proxy service
kubectl apply -f proxysvc.yaml
