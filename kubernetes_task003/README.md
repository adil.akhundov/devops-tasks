#task1 (add taint to master and add tolerance to pod)
kubectl taint node node1 author=adil:NoSchedule
kubectl apply -f podwithtolerations.yaml

#task2 (create namespace quota and create pod)
kubectl apply -f nsquota.yaml
kubectl apply -f nginx.yaml #this is sixth pod on namespace

#task3 (create daemonset with toleration)
kubectl apply -f fluentd_ds.yaml

#task4 (create static pod worker node)
cp test_staticpod.yaml /etc/kubernetes/manifests/test_staticpod.yaml
