#Create web containers

docker run -d --name webblue -v /data1/indexblue.html:/usr/share/nginx/html/index.html nginx

docker run -d --name webred -v /data2/indexblue.html:/usr/share/nginx/html/index.html nginx

docker run -d --name webblack -v /data3/indexblue.html:/usr/share/nginx/html/index.html nginx

#Create proxy container

docker run -d --name proxy --link webblue --link webred --link webblack -p 8080:80 -v /data/nginx.conf:/etc/nginx/conf.d/default.conf nginx
