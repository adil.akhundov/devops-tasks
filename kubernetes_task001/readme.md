#create namespaces

kubectl create ns red

kubectl create ns blue

#create blue and red deployment and configmaps

kubectl apply -f blueconfmap.yaml

kubectl apply -f redconfmap.yaml

#create blue and red services

kubectl apply -f bluesvc.yaml

kubectl apply -f redsvc.yaml

#create configmap for proxy

kubectl apply -f proxyconfmap.yaml

#create proxy service
kubectl apply -f proxysvc.yaml
